#include <iostream>
#include <string>

class Car {
private:
    int wheels;
    std::string reg_number;
    std::string color;
    
public: 
  /*  Car(){
        reg_number="";
        color="red";
        wheels=4;
        std::cout<<"Beep"<<std::endl;
    }*/
    Car(std::string rn="", std::string c="", int w=4){
        reg_number=rn;
        color=c;
        wheels=w;
        std::cout<<"Beep2"<<std::endl;
    }
    Car(const Car & c){
        reg_number=c.reg_number;
        color=c.color;
        wheels=c.wheels;
        std::cout<<"copy ctor"<<std::endl;
    }
};

int main(){
    Car car;
    Car car2("THX343","blue",4);
    Car car3(car2);
}
