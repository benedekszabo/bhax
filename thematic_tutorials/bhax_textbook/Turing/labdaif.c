#include <stdio.h>
#include <curses.h>
#include <unistd.h>

int main ( void )
{
    WINDOW *ablak;
    ablak = initscr();

    int x = 0;
    int y = 0;

    int deltax = 1;
    int deltay = 1;

    int mx;
    int my;

    while(1) 
    {
        getmaxyx ( ablak, my , mx );

        mvprintw ( y, x, "O" );

        refresh ();
        usleep ( 80000 );
        
        clear();

        x = x + deltax;
        y = y + deltay;

        if ( x>=mx-1 )         //jobb oldal
            deltax = -deltax;

        if ( x<=0 )            //bal oldal
            deltax = -deltax;
        
        if ( y<=0 )            //teteje
            deltay = -deltay;
        
        if ( y>=my-1 )         //alja
            deltay = -deltay;
        

    }
    return 0;
}
