#include "std_lib_facilities.h"
#include<unistd.h>
using namespace std;

void rajzol(int X,int Y,int sz,int m)
{
    cout<<' ';					//első '_' sor
    for(int i=0;i<sz+1;i++) cout<<'_';
    cout<<'\n';

    for(int i=0;i<Y;i++)   			//labda előtt
    {
    cout<<'|';
    for(int j=0;j<sz+1;j++) cout<<' ';
    cout<<'|';
    cout<<'\n';
    }

    cout<<'|';					//labda sora
    for(int i=0;i<X;i++) cout<<" ";
    cout<<'o';
    for(int i=X;i<sz;i++) cout<<" ";
    cout<<'|';
    cout<<'\n';

    for(int i=Y;i<m;i++)     			//labda után
    {
    cout<<'|';
    for(int j=0;j<sz+1;j++) cout<<' ';
    cout<<'|';
    cout<<'\n';
    }

    cout<<' ';
    for(int i=0;i<sz+1;i++) cout<<'T';
    cout<<endl; 				//záró '_' sor

}

int main()
{
    int x=0, y=0, szelesseg,magassag;

    cout<<"Add meg a palya meretet! (Ctrl + C-kilepes) \n";
    cin>>szelesseg>>magassag;
    szelesseg--;
    magassag--;
    
    while(1)
    {
	system("clear");
	x++; y++;
        rajzol(abs(szelesseg-(x%(szelesseg*2))),abs(magassag-(y%(magassag*2))),szelesseg,magassag); 
	usleep(200000);
    }

}
