/*
a kódot az itt található kódra alapoztam: https://drive.google.com/file/d/12ahRzjFAYkBWgYpLjjAlWxhHzq6XKFIe/view?fbclid=IwAR03L0uUTpcXfNbAN_C4HYSb1PV7PhHH3Y79K8-gyAWXULJGUu_t_JuzMf0
Credits: Kovács Ferencz
*/


#include<iostream>
#include<fstream>
#include<vector>
#include<string>
#include <algorithm>
#include <cctype>
using namespace std;

class LeetCipher {
public:
	LeetCipher();
	string decipher(string& text);
private:
	vector<vector <string>> leetabc;
	vector<string> abc;
};

	LeetCipher::LeetCipher() {
	srand (time(NULL));
leetabc.resize(26);
leetabc[0]= {"4", "/-||", "/_||", "@", "/||"};
leetabc[1]={"8","|3", "13", "|}", "|:", "|8", "18", "6", "|B", "|8", "lo", "|o"};
leetabc[2]= { "<", "{", "[", "("};
leetabc[3] = {"|)", "|}", "|]"};
leetabc[4] = {"3"};
leetabc[5] = { "|=", "ph", "|#", "||"};
leetabc[6] = {"[", "-", "[+", "6"};
leetabc[7] = {"4", "|-|", "[-]", "{-}", "|=|", "[=]", "{=}"};
leetabc[8] = {"1", "|", "!", "9"};
leetabc[9] = {"_|", "_/", "_7", "_)", "_]", "_}"};
leetabc[10] = {"|<", "1<", "l<", "|{", "l{"};
leetabc[11] = {"|_", "|", "1", "]["};
leetabc[12] = {"44", "|||/|", "^^", "/||/||", "/X||", "[]||/][", "[]V[]","][||||//][", "(V)","//., .||||", "N||"};
leetabc[13] = {"||||", "/||/", "/V", "][||||]["};
leetabc[14] ={"0", "()", "[]", "{}", "<>"};
leetabc[15] = { "|o", "|O", "|>", "|*", "||textdegree{}", "|D", "/o"};
leetabc[16] = {"O_", "9", "(,)", "0,"};
leetabc[17] = {"|2"," 12", ".-", "|^", "l2"};
leetabc[18] = {"5", "$", "§"};
leetabc[19] = {"7", "+", "7‘", "|’||’" , "‘|‘" , "~|~" , "-|-"};
leetabc[20] = {"|_|", "||_||", "/_/", "||_/", "(_)", "[_]", "{_}"};
leetabc[21] = {"||/"};
leetabc[22] = {"||/||/", "(/||)", "||^/", "|/|||", "||X/", "|||||’", "|’//", "VV"};
leetabc[23] = {"%", "*", "><", "}{", ")("};
leetabc[24] = {"‘/", "$|yen$"};
leetabc[25] = { "2", "7_", ">_"};

abc =
{
	"A" ,
	"B",
	"C",
	"D",
	"E",
	"F",
	"G",
	"H",
	"I",
	"J",
	"K",
	"L",
	"M",
	"N",
	"O",
	"P",
	"Q",
	"R",
	"S",
	"T",
	"U",
	"V",
	"W",
	"X",
	"Y",
	"Z"
};
}

string LeetCipher::decipher(string &text) {

	string leet = "";
	string be = text;

	transform(be.begin(), be.end(),be.begin(), ::toupper);

	while(!be.empty())
	{
	bool find=false;
	for (int i=0; i<abc.size(); i++)
		{	
		size_t found = be.find(abc[i]);
		if (found==0)
			{
			find =true;
			leet+=leetabc[i][rand() % leetabc[i].size()];
			if(be.length() > 1)
			be=be.substr(1);
		else
			be.clear();
		}
	}

	if(!find){
		leet+=be[0];
		if(be.length()>1)
			be=be.substr(1);
		else
			be.clear();
		}
	}

return leet;
}

int main(int argc, char * argv[])
{
	ifstream infile (argv[1]);
	fstream outfile (argv[2], ios::out);

	string text = "";
	string temp = "";

	while(getline(infile, temp)) {
		text+=temp;
	}

	LeetCipher* cipher = new LeetCipher();
	string cipheredText = cipher->decipher(text);
	outfile<<cipheredText<<endl;
}
