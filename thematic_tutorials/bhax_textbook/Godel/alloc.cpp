#include <iostream>
#include <exception>
#include <iostream>
#include <cxxabi.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <vector>
#include <unistd.h>
using namespace std;
/*
template <typename type>
struct Alloc: allocator<type>{
    type=allocate(size_t size){
        cout<<"Allokalas merete: "<<size<<endl;
        return new type(size);
    }
    void deallocate(type *ptr,size_t size){
        cout<<"Deallokalas merete: "<<size<<endl;
        delete[] ptr;
    };
};*/
template<typename T>
struct CustomAlloc{

	using size_type = size_t;
	using value_type = T;
	using pointer = T*;
	using const_pointer = const T*;
	using reference = T&;
	using const_reference = const T&;
	using difference_type = ptrdiff_t;

	CustomAlloc(){}
	CustomAlloc(const CustomAlloc &){}
	~CustomAlloc(){}
	pointer allocate (size_type n) {
		//++arena.nnn;
		int s;
		char * p = abi::__cxa_demangle (typeid(T).name(),0,0,&s);

		std::cout << "Allocating "
				  << n << " object of "
				  << n*sizeof(T)
				  << "  bytes. "
				  << typeid(T).name() << " = " << p
				  << std::endl;
		free (p);

		return reinterpret_cast<T*> (
            new char[n*sizeof(T)]
		);
	}

	void  deallocate (pointer p, size_type n) {
		delete[] reinterpret_cast<char *>(p);
		std::cout<<" deallocate ";
    }
};

int main()
{
    vector<int, CustomAlloc<int> > v1;
    for (int i=1;i<17;i++) v1.push_back(i);

   // getchar();
    return 0;
}
