package model;


/**
* @generated
*/
public class Motor extends Jármű {
    
    /**
    * @generated
    */
    private String Szín;
    
    /**
    * @generated
    */
    private Integer Lökettérfogat;
    
    
    
    /**
    * @generated
    */
    public String getSzín() {
        return this.Szín;
    }
    
    /**
    * @generated
    */
    public String setSzín(String Szín) {
        this.Szín = Szín;
    }
    
    /**
    * @generated
    */
    public Integer getLökettérfogat() {
        return this.Lökettérfogat;
    }
    
    /**
    * @generated
    */
    public Integer setLökettérfogat(Integer Lökettérfogat) {
        this.Lökettérfogat = Lökettérfogat;
    }
    
}

