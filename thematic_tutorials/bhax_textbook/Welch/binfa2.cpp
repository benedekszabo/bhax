#include "std_lib_facilities.h"
using namespace std;

struct elem
{
    elem * bal;
    elem * jobb;
};

void kiir_char(vector<char> V);
void beepit(elem* &p, elem* Gyoker, int e);
void bejar(elem* p,int szint);
void bejar2(elem* p,int szint);
void agak(elem* p,int szint,bool h);

int elemszam=0, osszeg=0, ag_db=0, aho=0, magassag=0;//aho=agak hosszanak osszege
double szoras_osszeg=0;

main()
{
    vector<char> v;
    elem* gyoker=new elem;
    gyoker->bal=nullptr;
    gyoker->jobb=nullptr;

    elem* pointer=gyoker;
    char x;
    int n=0;

    //ifstream f("be.txt");//
    while (cin>>x)//not f.eof())//
    {
     //   cin>>x;//f>>x;//
        switch(x)
        {
            case '0':   beepit(pointer,gyoker,0);
                        if (n<1000)
                        {
                            v.push_back(x);
                            n++;
                        }
                        break;

            case '1':   beepit(pointer,gyoker,1);
                        if (n<1000)
                        {
                            v.push_back(x);
                            n++;
                        }
                        break;

            default:    cout<<"Érvénytelen bemenet\n";
                        break;
        }

    }
    //v.pop_back();//

    bejar(gyoker,0);
    bejar2(gyoker,0);

    if (n<1000)
    {
        cout<<"Input adatok:";
        kiir_char(v);
    }
    cout<<"\n\nA fa againak szama: "<<ag_db<<endl;
    cout<<"\nA fa againak atlagos hossza: "<<setprecision(9)<<(double)aho/ag_db<<endl;
    cout<<"\nAz agak hosszanak szorasa: "<<sqrt(szoras_osszeg/ag_db)<<endl;
    cout<<"\nA korrigalt tapasztalati szoras: "<<sqrt(szoras_osszeg/(ag_db-1))<<endl;
    cout<<"\nA fa elemszama (gyokerelem nelkul): "<<elemszam<<endl;
    cout<<"\nA fa magassaga (gyokerelem nelkul): "<<magassag<<endl;
    cout<<"\nA fa elemeinek atlaga: (gyokerelem nelkul): "<<setprecision(9)<<(double)osszeg/elemszam<<endl;

    if (ag_db<=50)
    {
        cout<<"\nA fa agai:"<<endl;
        agak(gyoker,0,false);
    }


}

void kiir_char(vector<char> V)
{
    for (int i=0;i<V.size();i++)
    {
        if (i%60==0) cout<<'\n';
        cout<<V[i] ;
    }
}

void beepit(elem* & p, elem* Gyoker, int e)
{
    if (e==0)
    {
        if (p->bal!=nullptr) p=p->bal;
        else
        {
            elem* uj=new elem;
            uj->bal=nullptr;
            uj->jobb=nullptr;
            p->bal=uj;
            p=Gyoker;
            elemszam++;
        }
    }
    else
    {
        if (p->jobb!=nullptr) p=p->jobb;
        else
        {
            elem* uj=new elem;
            uj->bal=nullptr;
            uj->jobb=nullptr;
            p->jobb=uj;
            p=Gyoker;
            elemszam++;
            osszeg++;
        }
    }
}
void bejar(elem* p,int szint)
{
    if (p->bal==nullptr and p->jobb==nullptr)
    {
        ag_db++;
        aho+=szint;
        if (szint>magassag) magassag=szint;
    }
    else
    {
        if (p->bal!=nullptr) bejar(p->bal,szint+1);
        if (p->jobb!=nullptr) bejar(p->jobb,szint+1);
    }
}

void bejar2(elem* p,int szint)
{
    if (p->bal==nullptr and p->jobb==nullptr)
    {
        szoras_osszeg+=(szint-((double)aho/ag_db))*(szint-((double)aho/ag_db));
    }
    else
    {
        if (p->bal!=nullptr) bejar2(p->bal,szint+1);
        if (p->jobb!=nullptr) bejar2(p->jobb,szint+1);
    }
}

int ag[100];
bool honnan;
void agak(elem* p,int szint,bool h)
{

    if (h) ag[szint]=0;
    else  ag[szint]=1;

    if (p->bal==nullptr and p->jobb==nullptr)
    {
       for (int j=1;j<=szint;j++) cout<<ag[j];
       cout<<endl;
    }
    else
    {
        if (p->bal!=nullptr) agak(p->bal,szint+1,true);
        if (p->jobb!=nullptr) agak(p->jobb,szint+1,false);
    }
}

