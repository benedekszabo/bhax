#include <stdio.h>

int f(int a, int b)
{
    return a+b;
}

int main()
{

    int a=3;

    printf("%d, %d", f(a, ++a), f(++a, a));

    printf("\n");

    return 0;
}
