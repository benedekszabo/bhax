<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Conway!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>
    <section>
        <title>Hangyaszimulációk</title>
        <para>
            Írj Qt C++-ban egy hangyaszimulációs programot, a forrásaidról utólag reverse engineering jelleggel
            készíts UML osztálydiagramot is!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://bhaxor.blog.hu/2018/10/10/myrmecologist">https://bhaxor.blog.hu/2018/10/10/myrmecologist</link>
        </para>
        <para>
            Megoldás forrása:<link xlink:href="Conway/hangya">
                <filename>bhax/thematic_tutorials/bhax_textbook/Conway/hangya</filename>
            </link>                     
        </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para>   
<para>
A használat előtt ne felejtsük el a következő paranccsal telepíteni a szükséges eszközöket: <command>sudo apt-get install qtbase5-dev</command>.
</para>
<para>
A kód hangyák kommunikációját szimulálja. A hangyák feromonokkal kommunikálnak egymásssal. Cellákra osztjuk a tartományunkat (képernyő). A hangyák azt a szomszédjukat keresik, akinek a feromonja a legerősebb, erre indulnak el. Az idő teltével csökken a cellák feromonszintje, de amennyiben egy hangya belelép, megnövekszik. Parancssori argumentumokon keresztül tudjuk az értékeket megadni, mégpedig a következő formában:
        </para>
        <para>
            A <filename>main.cpp</filename> szerinti futtatás:
            </para>
        <para>
./myrmecologist -w 250 -m 150 -n 400 -t 10 -p 5 -f 80 -d 0 -a 255 -i 3 -s 3  -c 22
            </para>
        <para>
A következőket tudjuk így megadni:
</para>
<para>
w: hány oszlopból álljon egy cella, a szélessége
</para>
<para>
m: a cellák magassága
</para>
<para>
n: hangyák száma
</para>
<para>
t: lépések sűrűsége, milliszekundumban
</para>
<para>
p: feromon párolgása
</para>
<para>
f: egy hangya feromonja
</para>
<para>
d: kezdő feromonkérték cellánként
</para>
<para>
a: maximális feromonérték
</para>
<para>
i: minimális feromonkérték
</para>
<para>
s: szomszédos celláákba mennyi feromon jut
</para>
<para>
c: maximum hány hangya lehet egy cellában.
</para>
<para>
Ezek megadása nem kötelező, alapértelmezett értékek vannak megadva.
</para> 

<para>
Nézzük meg a forráskódot! Ehhez készítettem egy úgynevezett UML osztálydiagramot, aminek segítségével könnyebben átlátható egy bonyolult program, még akkor is, ha több fájlban van konkrétan. Egy téglalap egy osztálynak felel meg, benne pedig a tagváltozók és alattuk a tagfüggvények. A '-' jel a private, a '+' jel a publikus tulajdonságra utal. (megj: a diagram némileg hibás, az Ant osztály tagjai publikusak, míg az AntThread osztzály alsó hat függvénye privát)
	<figure>
                <title>UML osztálydiagram</title>
                <mediaobject>
                    <imageobject>
                        <imagedata fileref="./Conway/hangya/UML Class Diagram.png" format="PNG"/>
                    </imageobject>
                </mediaobject>
            </figure>

</para>
<para>
Az Ant osztály, az ant.h-ban található:
</para>
<programlisting language="c++"><![CDATA[
class Ant
{

public:
    int x;
    int y;
    int dir;

    Ant(int x, int y): x(x), y(y) {
        
        dir = qrand() % 8;
        
    }

};]]>
            </programlisting>

<para>
Egy hangya egyed leírása. A hangyák koordinátáit határozzuk meg, illetve a dir-be kiszámoljuk a hangya irányát, ami véletlenszerű.
</para>
<programlisting language="c++"><![CDATA[
typedef std::vector<Ant> Ants;
]]></programlisting>

<para>
Ugyanabban a fájlban van ez a sor, ami arra szolgál, hogy az Ant-okból álló vektorra csupán Ants-ként tudjunk hivatkozni. vagyis a szóköz baloldala helyett használhatjuk a jobb oldalát, bárhol, ahol a bal oldalt használnánk.
</para>
<para>
Az AntWin osztály, mely az antwin.h-ban található: 
</para>
<programlisting language="c++"><![CDATA[
#include <QMainWindow>
#include <QPainter>
#include <QString>
#include <QCloseEvent>
#include "antthread.h"
#include "ant.h"

class AntWin : public QMainWindow
{
    Q_OBJECT

public:
    AntWin(int width = 100, int height = 75,
           int delay = 120, int numAnts = 100,
           int pheromone = 10, int nbhPheromon = 3,
           int evaporation = 2, int cellDef = 1,
           int min = 2, int max = 50,
           int cellAntMax = 4, QWidget *parent = 0);

    AntThread* antThread;

    void closeEvent ( QCloseEvent *event ) {

        antThread->finish();
        antThread->wait();
        event->accept();
    }

    void keyPressEvent ( QKeyEvent *event )
    {

        if ( event->key() == Qt::Key_P ) {
            antThread->pause();
        } else if ( event->key() == Qt::Key_Q
                    ||  event->key() == Qt::Key_Escape ) {
            close();
        }

    }

    virtual ~AntWin();
    void paintEvent(QPaintEvent*);

private:

    int ***grids;
    int **grid;
    int gridIdx;
    int cellWidth;
    int cellHeight;
    int width;
    int height;
    int max;
    int min;
    Ants* ants;

public slots :
    void step ( const int &);

};]]>
            </programlisting>
<para>
A sok include-ra elöl a Qt miatt van szükség, ezek kellenek a használatához. A QMainWindow arra szolgál, hogy ez legyen a fő ablak, amit látunk. A 
QPainter a kirajzoláshoz szükséges, a QString a stringek kezeléséhez,a QCloseEvent pedig a befejezéshez. Mivel ez lesz a fő ablak, itt is kapcsoljuk össze a 
a másik két headert is (ant.h, antthread.h). Ezután megadjuk az alapértelmezett érékeket. A keyPressEvent() függvényben megadjuk azt is, hogy ha a felhasználó a p-billentyűt lenyomja, a program megáll, illetve a q billentyű hatására kilép.

</para>
<para>
A program főbb számításait az antthread.cpp és az antthread.h végzik, mivel most leginkább az osztályokkal foglalkozunk, nézzük a headert:
</para>
<programlisting language="c++"><![CDATA[
#include <QThread>
#include "ant.h"

class AntThread : public QThread
{
    Q_OBJECT

public:
    AntThread(Ants * ants, int ***grids, int width, int height,
             int delay, int numAnts, int pheromone, int nbrPheromone, 
             int evaporation, int min, int max, int cellAntMax);
    
    ~AntThread();
    
    void run();
    void finish()
    {
        running = false;
    }

    void pause()
    {
        paused = !paused;
    }

    bool isRunnung()
    {
        return running;
    }

private:
    bool running {true};
    bool paused {false};
    Ants* ants;
    int** numAntsinCells;
    int min, max;
    int cellAntMax;
    int pheromone;
    int evaporation;
    int nbrPheromone;
    int ***grids;
    int width;
    int height;
    int gridIdx;
    int delay;
    
    void timeDevel();

    int newDir(int sor, int oszlop, int vsor, int voszlop);
    void detDirs(int irany, int& ifrom, int& ito, int& jfrom, int& jto );
    int moveAnts(int **grid, int row, int col, int& retrow, int& retcol, int);
    double sumNbhs(int **grid, int row, int col, int);
    void setPheromone(int **grid, int row, int col);

signals:
    void step ( const int &);

};]]>
            </programlisting>
<para>
A szálak kezelése miatt szükségünk van a Qthread osztályra. Az AntThread konstruktorába kerülnek az AntWin-ben megadott értékek, majd a hangyák mozgatása következik: a feromonszint alapján (ha nincs telel a cella) kiszámolja az irányt, amerre indul a hangya, majd az AntWin osztály tudni fogja, hogy merre kell a cellát színezni.
</para>


<para>
A futtatáshoz legyen minden fájl (7 db) egy mappában, itt kell álljunk. A következő parancsokat kell kiadnunk: <command>qmake myrmecologist.pro</command>, 
                    <command>make</command>, 
                    <command>./myrmecologist (illetve kapcsolók, ha kell)</command>

</para>

<figure>
                <title>Kapcsolók nélkül</title>
                <mediaobject>
                    <imageobject>
                        <imagedata fileref="./Conway/hangya/sima.png" format="PNG"/>
                    </imageobject>
                </mediaobject>
            </figure>
            <figure>
                <title>Kapcsolókkal</title>
                <mediaobject>
                    <imageobject>
                        <imagedata fileref="./Conway/hangya/kapcsolok.png" format="PNG"/>
                    </imageobject>
                </mediaobject>
            </figure>

    </section>        
    <section>
        <title>Java életjáték</title>
        <para>
            Írd meg Java-ban a John Horton Conway-féle életjátékot, 
            valósítsa meg a sikló-kilövőt!
        </para>
        <para>
            Megoldás videó: 
        </para>
        <para>
            Megoldás forrása:   <link xlink:href="Conway/game_of_life.java">
                <filename>bhax/thematic_tutorials/bhax_textbook/Conway/game_of_life.java</filename>
            </link>                      
        </para>
<para>
Tutorom: <link xlink:href="https://gitlab.com/fupn26/bhax">Fürjes-Benke Péter</link>, a forráskódot is tőle kaptam.
</para>
        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para> 


<para>
Használat előtt, ha még nem tettük meg, telepítsük a szükséges eszközöket: sudo apt-get install openjdk-8-jdk.
</para>
<para>
Conway 1970-ben saját sejtautomatáját készített. Mi is ez tulajdonképpen? Lényege, hogy mi adunk meg bizonyos feltételeket az életben maradásra, a halálra, illetve a születésre. Ezután megfigyeljük az általunk létrehozott, teremtett lényeket. A mi példánkban Conway 3 szabályát felhasználó automata szerepel, mely mai napig az egyik legnépszerűbb.
</para>
<para>
			A szabályok a következők:
</para>
			<para>
1. szabály: Egy sejt csak akkor marad életbe, ha 2 vagy 3 szomszédja van.
2. szabály: Ha egy sejtnek 3-nál több szomszédja van, túlnépesedés miatt
			meghal. Ha pedig 2-nél kevesebb a szomszédai száma, akkor
			pedig az elszigetelődés miatt hal meg.
3. szabály: Egy sejt megszületik, ha az üres cellának 3 élő sejt
			található a szomszédos cellákban.
			</para>
<para>
Ilyen szabályokkal nagyon érdekes animációk születhetnek, de minket leginkább az egyik érdekel, az az eset, amikor fix cellákba helyezzük a sejteket, és ezután elindulnak az úgynevezett siklók, ezt siklókilövőnek is szokás nevezni.
</para>	
<programlisting language="java"><![CDATA[	
import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.ImageObserver;
import java.text.AttributedCharacterIterator;
import java.util.ArrayList;
import java.awt.Event;

public class game_of_life extends JFrame {
    RenderArea ra;
    private int i;
    
    public game_of_life() {
        super("Game of Life");
        this.setSize(1005, 1030);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setVisible(true);
        this.setResizable(false);
        ra = new RenderArea();
        ra.setFocusable(true);
        ra.grabFocus();
        add(ra);
        int[][] siklokilovo = {{6,0},{6,1},
            {7,0},{7,1},
            {3,13},
            {4,12},{4,14},
            {5,11},{5,15},{5,16},{5,25},
            {6,11},{6,15},{6,16},{6,22},{6,23},{6,24},{6,25},
            {7,11},{7,15},{7,16},{7,21},{7,22},{7,23},{7,24},
            {8,12},{8,14},{8,21},{8,24},{8,34},{8,35},
            {9,13},{9,21},{9,22},{9,23},{9,24},{9,34},{9,35},
            {10,22},{10,23},{10,24},{10,25},
            {11,25}};
        int min_o = 5;
        int min_s = 85;
        for (int i = 0; i < siklokilovo.length; ++i)
        {
          ra.entities.get(min_o + siklokilovo[i][1]).set(min_s+ siklokilovo[i][0],!ra.entities.get(min_o + siklokilovo[i][1]).get((min_s+ siklokilovo[i][0])));
                this.update(this.getGraphics());  
        }
        

        ra.edit_mode = false;
        ra.running = true;
    }

    public void update() {
        ArrayList<ArrayList<Boolean>> entities = new ArrayList<ArrayList<Boolean>>();// = ra.entities;
        int size1 = ra.entities.size();
        int size2 = ra.entities.get(0).size();
        for(int i=0;i<size1;i++)
        {
            entities.add( new ArrayList<Boolean>());
            for(int j=0;j<size2;j++)
            {
                int alive = 0;
                
                if(ra.entities.get((size1+i-1)%size1).get((size2+j-1)%size2)) alive++;
                if(ra.entities.get((size1+i-1)%size1).get((size2+j)%size2)) alive++;
                if(ra.entities.get((size1+i-1)%size1).get((size2+j+1)%size2)) alive++;

                if(ra.entities.get((size1+i)%size1).get((size2+j-1)%size2)) alive++;
                if(ra.entities.get((size1+i)%size1).get((size2+j+1)%size2)) alive++;

                if(ra.entities.get((size1+i+1)%size1).get((size2+j-1)%size2)) alive++;
                if(ra.entities.get((size1+i+1)%size1).get((size2+j)%size2)) alive++;
                if(ra.entities.get((size1+i+1)%size1).get((size2+j+1)%size2)) alive++;
                


                /*for(int k=-1;i<2;k++)
                {
                    for(int l = -1; l < 2 ;l++)
                    {
                        if(!(k==0 && l == 0))
                        {
                            if(ra.entities.get((size1+i+k)%size1).get((size2+j+l)%size2)) alive++;
                        }
                    }
                }*/

                if(ra.entities.get(i).get(j))
                {
                    if(alive < 2 || alive > 3)
                    {
                        //ra.entities.get(i).set(j,false);
                        entities.get(i).add(false);
                    }
                    else
                    {
                        entities.get(i).add(true);
                    }
                }
                else
                {
                    if(alive == 3)
                    {
                        //ra.entities.get(i).set(j,true);
                        entities.get(i).add(true);
                    }
                    else
                    {
                        entities.get(i).add(false);
                    }
                }

            }
        }
        ra.entities = entities;

    }

    class RenderArea extends JPanel implements KeyListener {
        public ArrayList<ArrayList<Boolean>> entities;

        public int diff;
        public boolean edit_mode;
        public boolean running;
        public RenderArea() {
            super();
            setSize(1000, 1000);
            setVisible(true);
            setBackground(Color.WHITE);
            setForeground(Color.BLACK);
            setLocation(0, 0);

            diff = 10;
            

            /*this.addMouseListener((MouseListener) new MouseListener(){
            
                @Override
                public void mouseReleased(MouseEvent arg0) {
                    
                }
            
                @Override
                public void mousePressed(MouseEvent arg0) {
                    clicked(arg0);
                }
            
                @Override
                public void mouseExited(MouseEvent arg0) {
                    
                }
            
                @Override
                public void mouseEntered(MouseEvent arg0) {
                    
                }
            
                @Override
                public void mouseClicked(MouseEvent arg0) {
                    
                }
            });*/ 
            this.addKeyListener(this);
            entities = new ArrayList<ArrayList<Boolean>>();
            for(int i=0;i<1000/diff;i++)
            {
                entities.add(new ArrayList<Boolean>());
                for(int j=0;j<1000/diff;j++)
                {
                    entities.get(i).add(false);
                }
            }
            
        }

        /*void clicked(MouseEvent arg0)
        {
            System.out.println("Button "+(arg0.getButton()== 1 ? "Left" : "Right"));
            System.out.println("X:"+arg0.getX()/diff);
            System.out.println("Y:"+arg0.getY()/diff);
            if(edit_mode)
            {
                entities.get(arg0.getX()/diff).set(arg0.getY()/diff,!entities.get(arg0.getX()/diff).get((arg0.getY()/diff)));
                this.update(this.getGraphics());
            }
            
        }*/


        @Override
        public void keyTyped(KeyEvent e) {
            //System.out.println(e.getKeyChar());
        }
    
        @Override
        public void keyReleased(KeyEvent e) {
            System.out.println("Key pressed:"+e.getKeyChar());
            if(e.getKeyChar()=='e')
            {
                edit_mode = !edit_mode;
            }
            else if(e.getKeyChar()=='q')
            {
                this.running = false;
            }
            else if(e.getKeyChar()=='c')
            {
                if(edit_mode)
                {
                    for(int i=0;i<this.entities.size();i++)
                    {
                        for(int j=0;j<this.entities.get(1).size();j++)
                        {
                            this.entities.get(i).set(j,false);
                        }
                    }
                    this.update(this.getGraphics());
                }
                
            }

        }
    
        @Override
        public void keyPressed(KeyEvent e) {
            //System.out.println(e.getKeyChar());
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            g.clearRect(0, 0, 1000, 1000);
            for(int i=0;i<1000;i+=diff)
            {
                g.drawLine(i, 0, i, 1000);
            }
            for(int j=0;j<1000;j+=diff)
            {
                g.drawLine(0, j, 1000, j);
            }
            for(int i=0;i<1000;i+=diff)
            {
                for(int j=0;j<1000;j+=diff)
                {
                    if(entities.get(i/diff).get(j/diff))
                    {
                        g.setColor(Color.BLACK);
                    
                    }
                    else
                    {
                        g.setColor(Color.WHITE);
                    }
                    
                    g.fillRect(i+2, j+2, diff-3, diff-3);
                }
            }
        }

        

        private static final long serialVersionUID = 1L;
        
    }

    private static final long serialVersionUID = 1L;
    public static void main(String args[])
    {
        game_of_life gol = new game_of_life();
        while(gol.ra.running)
        {
            if(!gol.ra.edit_mode)gol.update();
            try{Thread.sleep(200);}
            catch(Exception ex)
            {

            }
            gol.ra.update(gol.ra.getGraphics());
        }
        gol.dispose();
    }
}
	]]>
            </programlisting>
<para>		
fordítás: javac game_of_life.java
</para>
<para>
futtatás: java game_of_life
				
</para>
<figure>
                <title>Siklókilövő</title>
                <mediaobject>
                    <imageobject>
                        <imagedata fileref="./Conway/gameoflife.png" format="PNG"/>
                    </imageobject>
                </mediaobject>
            </figure>

           
    </section>        
    <section>
        <title>Qt C++ életjáték</title>
        <para>
            Most Qt C++-ban!
        </para>
        <para>
            Megoldás videó: 
        </para>
        <para>
            Megoldás forrása: <link xlink:href=""></link>               
        </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para>    
		
<para>
Használathoz most is szükséges az alábbi telepítés, ha még nem tettük volna ezidáig meg: 
sudo apt-get install libqt4-dev. Fordítás, futtatás az alábbi parancsokkal:
qmake--Qt sejtkilövő.pro--
make--
./Qt sejtkilövő
</para>
		
<para>
Most az előző feladatban Java-ban megírt Conway-féle életjáték C++-beli implementációját fogjuk elkészíteni. A Qt grafikus felületet fogjuk ezuttal is használni. Annyi a külöbség, hogy már adottak a koordináták. Viszint ugyanúgy siklókilövőt készítünk, ugyanazokkal a szabályokkal:			
</para>
<para>
1. szabály: Egy sejt csak akkor marad életbe, ha 2 vagy 3 szomszédja van.
2. szabály: Ha egy sejtnek 3-nál több szomszédja van, túlnépesedés miatt
			meghal. Ha pedig 2-nél kevesebb a szomszédai száma, akkor
			pedig az elszigetelődés miatt hal meg.
3. szabály: Egy sejt megszületik, ha az üres cellának 3 élő sejt
			található a szomszédos cellákban.
</para>
<para>
Azért, hogy a kód átláthatóbb legyen, négy külön állományban van a program, a classok külön vannak deklarálva.
</para>
<programlisting language="c++"><![CDATA[
#include <QtGui/QMainWindow>
#include <QPainter>
#include "sejtszal.h"

class SejtSzal;

class SejtAblak : public QMainWindow
{
    Q_OBJECT

public:
    SejtAblak(int szelesseg = 100, int magassag = 75, QWidget *parent = 0);
    ~SejtAblak();
    // Egy sejt lehet élõ
    static const bool ELO = true;
    // vagy halott
    static const bool HALOTT = false;
    void vissza(int racsIndex);

protected:
    // Két rácsot használunk majd, az egyik a sejttér állapotát
    // a t_n, a másik a t_n+1 idõpillanatban jellemzi.
    bool ***racsok;
    // Valamelyik rácsra mutat, technikai jellegû, hogy ne kelljen a
    // [2][][]-ból az elsõ dimenziót használni, mert vagy az egyikre
    // állítjuk, vagy a másikra.
    bool **racs;
    // Megmutatja melyik rács az aktuális: [rácsIndex][][]
    int racsIndex;
    // Pixelben egy cella adatai.
    int cellaSzelesseg;
    int cellaMagassag;
    // A sejttér nagysága, azaz hányszor hány cella van?
    int szelesseg;
    int magassag;    
    void paintEvent(QPaintEvent*);
    void siklo(bool **racs, int x, int y);
    void sikloKilovo(bool **racs, int x, int y);

private:
    SejtSzal* eletjatek;

};
void SejtAblak::sikloKilovo(bool **racs, int x, int y) {

    racs[y+ 6][x+ 0] = ELO;
    racs[y+ 6][x+ 1] = ELO;
    racs[y+ 7][x+ 0] = ELO;
    racs[y+ 7][x+ 1] = ELO;

    racs[y+ 3][x+ 13] = ELO;

    racs[y+ 4][x+ 12] = ELO;
    racs[y+ 4][x+ 14] = ELO;

    racs[y+ 5][x+ 11] = ELO;
    racs[y+ 5][x+ 15] = ELO;
    racs[y+ 5][x+ 16] = ELO;
    racs[y+ 5][x+ 25] = ELO;

    racs[y+ 6][x+ 11] = ELO;
    racs[y+ 6][x+ 15] = ELO;
    racs[y+ 6][x+ 16] = ELO;
    racs[y+ 6][x+ 22] = ELO;
    racs[y+ 6][x+ 23] = ELO;
    racs[y+ 6][x+ 24] = ELO;
    racs[y+ 6][x+ 25] = ELO;

    racs[y+ 7][x+ 11] = ELO;
    racs[y+ 7][x+ 15] = ELO;
    racs[y+ 7][x+ 16] = ELO;
    racs[y+ 7][x+ 21] = ELO;
    racs[y+ 7][x+ 22] = ELO;
    racs[y+ 7][x+ 23] = ELO;
    racs[y+ 7][x+ 24] = ELO;

    racs[y+ 8][x+ 12] = ELO;
    racs[y+ 8][x+ 14] = ELO;
    racs[y+ 8][x+ 21] = ELO;
    racs[y+ 8][x+ 24] = ELO;
    racs[y+ 8][x+ 34] = ELO;
    racs[y+ 8][x+ 35] = ELO;

    racs[y+ 9][x+ 13] = ELO;
    racs[y+ 9][x+ 21] = ELO;
    racs[y+ 9][x+ 22] = ELO;
    racs[y+ 9][x+ 23] = ELO;
    racs[y+ 9][x+ 24] = ELO;
    racs[y+ 9][x+ 34] = ELO;
    racs[y+ 9][x+ 35] = ELO;

    racs[y+ 10][x+ 22] = ELO;
    racs[y+ 10][x+ 23] = ELO;
    racs[y+ 10][x+ 24] = ELO;
    racs[y+ 10][x+ 25] = ELO;

    racs[y+ 11][x+ 25] = ELO;

}
#include <QThread>
#include "sejtablak.h"

class SejtAblak;

class SejtSzal : public QThread
{
    Q_OBJECT

public:
    SejtSzal(bool ***racsok, int szelesseg, int magassag,
             int varakozas, SejtAblak *sejtAblak);
    ~SejtSzal();
    void run();

protected:
    bool ***racsok;
    int szelesseg, magassag;
    // Megmutatja melyik rács az aktuális: [rácsIndex][][]
    int racsIndex;
    // A sejttér két egymást követõ t_n és t_n+1 diszkrét idõpillanata
    // közötti valós idõ.
    int varakozas;
    void idoFejlodes();
    int szomszedokSzama(bool **racs,
                        int sor, int oszlop, bool allapot);
    SejtAblak* sejtAblak;

};



void SejtSzal::idoFejlodes() {

    bool **racsElotte = racsok[racsIndex];
    bool **racsUtana = racsok[(racsIndex+1)%2];

    for(int i=0; i<magassag; ++i) { // sorok
        for(int j=0; j<szelesseg; ++j) { // oszlopok

            int elok = szomszedokSzama(racsElotte, i, j, SejtAblak::ELO);

            if(racsElotte[i][j] == SejtAblak::ELO) {
                /* Élõ élõ marad, ha kettõ vagy három élõ
             szomszedja van, különben halott lesz. */
                if(elok==2 || elok==3)
                    racsUtana[i][j] = SejtAblak::ELO;
                else
                    racsUtana[i][j] = SejtAblak::HALOTT;
            }  else {
                /* Halott halott marad, ha három élõ
             szomszedja van, különben élõ lesz. */
                if(elok==3)
                    racsUtana[i][j] = SejtAblak::ELO;
                else
                    racsUtana[i][j] = SejtAblak::HALOTT;
            }
        }
    }
    racsIndex = (racsIndex+1)%2;
}]]>
			</programlisting>
			
		
        
    </section>        
    <section>
        <title>BrainB Benchmark</title>
        <para>
        </para>
        <para>
            Megoldás videó: 
        </para>
        <para>
            Megoldás forrása: <link xlink:href=""></link>               
        </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para>  
<para>
Használat előtt, ha még nem tettük meg, telepítsük a szükséges eszközöket: sudo apt-get install libqt4-dev--
sudo apt-get install opencv-data--
sudo apt-get install libopencv-dev

</para>
<para>
A BrainB Benchmark esport tehetségek felkutatásával foglalkozik. Az agy kognitív képességeit vizsgálja, olyan feladattal, hogy utána a szerzett pont összemérhető legyen az emberek között. Lényegében a karakterelvesztést teszteli, azaz ha elveszítjük a karakterünket, mennyi idő elteltével találjuk meg, illetve ha megvan, meddig tart elveszteni.

</para>
<para>
A játék összesen 10 percig tart, és minél bonyolultabb kép lesz a végén, annál jobb vagy. A végén az eredmény egy fájlban lesz. A futtatás a már látott módon. Fontos, hogy egy mappában legyenek a szükséges fájlok. Kilépni menet közben is lehet, az Esc-el. Az addigi eredményeknek is meglesz a leírása. Fordítás, futtatás a következőképpen: 
          
            <function>qmake BrainB.pro</function>, <function>make</function>,
            <function>./BrainB</function>. 

</para>
            <programlisting language="c++"><![CDATA[
#include <QApplication>
#include <QTextStream>
#include <QtWidgets>
#include "BrainBWin.h"

int main ( int argc, char **argv )
{
        QApplication app ( argc, argv );

        QTextStream qout ( stdout );
        qout.setCodec ( "UTF-8" );
            
                qout << "\n" << BrainBWin::appName << QString::fromUtf8 ( " Copyright (C) 2017, 2018 Norbert Bátfai" ) << endl;
                QRect rect = QApplication::desktop()->availableGeometry();
           
                BrainBWin brainBWin ( rect.width(), rect.height() );
           
BrainBWin::BrainBWin ( int w, int h, QWidget *parent ) : QMainWindow ( parent )
{


        statDir = appName + " " + appVersion + " - " + QDate::currentDate().toString() + QString::number ( QDateTime::currentMSecsSinceEpoch() );

        brainBThread = new BrainBThread ( w, h - yshift );
        brainBThread->start();

        connect ( brainBThread, SIGNAL ( heroesChanged ( QImage, int, int ) ),
                  this, SLOT ( updateHeroes ( QImage, int, int ) ) );

        connect ( brainBThread, SIGNAL ( endAndStats ( int ) ),
                  this, SLOT ( endAndStats ( int ) ) );

}

BrainBThread::BrainBThread ( int w, int h )
{

        dispShift = heroRectSize+heroRectSize/2;

        this->w = w - 3 * heroRectSize;
        this->h = h - 3 * heroRectSize;

        std::srand ( std::time ( 0 ) );

        Hero me ( this->w / 2 + 200.0 * std::rand() / ( RAND_MAX + 1.0 ) - 100,
                  this->h / 2 + 200.0 * std::rand() / ( RAND_MAX + 1.0 ) - 100,
                                                         255.0 * std::rand() / ( RAND_MAX + 1.0 ), 9 );

        Hero other1 ( this->w / 2 + 200.0 * std::rand() / ( RAND_MAX + 1.0 ) - 100,
                      this->h / 2 + 200.0 * std::rand() / ( RAND_MAX + 1.0 ) - 100, 255.0 * std::rand() / ( RAND_MAX + 1.0 ), 5, "Norbi Entropy" );
        Hero other2 ( this->w / 2 + 200.0 * std::rand() / ( RAND_MAX + 1.0 ) - 100,
                      this->h / 2 + 200.0 * std::rand() / ( RAND_MAX + 1.0 ) - 100, 255.0 * std::rand() / ( RAND_MAX + 1.0 ), 3, "Greta Entropy" );
        Hero other4 ( this->w / 2 + 200.0 * std::rand() / ( RAND_MAX + 1.0 ) - 100,
                      this->h / 2 + 200.0 * std::rand() / ( RAND_MAX + 1.0 ) - 100, 255.0 * std::rand() / ( RAND_MAX + 1.0 ), 5, "Nandi Entropy" );
        Hero other5 ( this->w / 2 + 200.0 * std::rand() / ( RAND_MAX + 1.0 ) - 100,
                      this->h / 2 + 200.0 * std::rand() / ( RAND_MAX + 1.0 ) - 100, 255.0 * std::rand() / ( RAND_MAX + 1.0 ), 7, "Matyi Entropy" );

        heroes.push_back ( me );
        heroes.push_back ( other1 );
        heroes.push_back ( other2 );
        heroes.push_back ( other4 );
        heroes.push_back ( other5 );

}
Hero ( int x=0, int  y=0, int color=0, int agility=1,  std::string name ="Samu Entropy" )
    void move ( int maxx, int maxy, int env ) {

        int newx = x+ ( ( ( double ) agility*1.0 ) * ( double ) ( std::rand() / ( RAND_MAX+1.0 ) )-agility/2 ) ;
        if ( newx-env > 0 && newx+env < maxx ) {
            x = newx;
        }
        int newy = y+ ( ( ( double ) agility*1.0 ) * ( double ) ( std::rand() / ( RAND_MAX+1.0 ) )-agility/2 );
        if ( newy-env > 0 && newy+env < maxy ) {
            y = newy;
        }



class BrainBThread : public QThread
{
    Q_OBJECT

    
     //Norbi
    cv::Scalar cBg { 247, 223, 208 };
    cv::Scalar cBorderAndText { 47, 8, 4 };
    cv::Scalar cCenter { 170, 18, 1 };
    cv::Scalar cBoxes { 10, 235, 252 };
    Heroes heroes;
    int heroRectSize {40};

    cv::Mat prev {3*heroRectSize, 3*heroRectSize, CV_8UC3, cBg };
    int bps;
    long time {0};
    long endTime {10*60*10};
    int delay {100};

    bool paused {true};
    int nofPaused {0};

    std::vector<int> lostBPS;
    std::vector<int> foundBPS;

    int w;
    int h;
    int dispShift {40};]]>
            </programlisting>


          
    </section>        
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
</chapter>                
